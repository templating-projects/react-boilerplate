# 2. react-snapshot

Date: 2020-07-10

## Status

2020-07-10 proposed

2020-07-10 done

## Context

On veut servir des pages statiques, pour le SEO essentiellement. Mais on ne veut pas sortir l'artillerie lourde comme gatsby, ssr, etc.

## Decision

Nous allons utiliser __react-snapshot__ et voir si celà nous suffirait.  
Il se trouve que la mise en place ne nécessite rien de particulier.

## Consequences

* SEO friendly
* Gain de rapidité de chargement
* Pas de contrainte d'installation et d'usages
