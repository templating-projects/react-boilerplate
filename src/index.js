// From react
import React from 'react'
import { render } from 'react-snapshot'
import { Router } from '@reach/router'

// From internal
import Homepage from '~/pages/Homepage'
import Profile from '~/pages/Profile'

import '~/index.css'
import * as serviceWorker from '~/serviceWorker'

render(
  <React.StrictMode>
    <Router>
      <Homepage path='/' />
      <Profile path='/profile' />
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
