// From react
import React, { useState, useEffect } from 'react'

// From internal
import '~/pages/Homepage/styles.css'
import logo from '~/assets/images/logo.svg'

const name = 'Homepage'

const App = props => {
  const [mounted, setMounted] = useState(false)

  useEffect(() => {
    console.log(new Date(), name, 'mount')
  }, [])
  useEffect(() => () => {
    console.log(new Date(), name, 'unmount')
  }, [])

  const initialize = async () => {
    console.log(new Date(), name, 'initialize')
    setMounted(true)
  }
  if (!mounted) {
    initialize()
  }

  return (
    <div className='app'>
      <header className='app-header'>
        <img src={logo} className='app-logo' alt='logo' />
        <p>
          Edit <code>src/pages/Homepage/index.js</code> and save to reload.
        </p>
        <a className='app-link' href='https://reactjs.org' target='_blank' rel='noopener noreferrer'>
          Learn React
        </a>
      </header>
    </div>
  )
}

export default App
