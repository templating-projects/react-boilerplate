// From react
import React, { useState, useEffect } from 'react'
import { Helmet } from 'react-helmet'

// From internal
import '~/pages/Profile/styles.css'
import logo from '~/assets/images/logo.svg'

const name = 'Profile'

const App = props => {
  const [mounted, setMounted] = useState(false)

  useEffect(() => {
    console.log(new Date(), name, 'mount')
  }, [])
  useEffect(() => () => {
    console.log(new Date(), name, 'unmount')
  }, [])

  const initialize = async () => {
    console.log(new Date(), name, 'initialize')
    setMounted(true)
  }
  if (!mounted) {
    initialize()
  }

  return (
    <>
      <Helmet>
        <meta charSet='utf-8' />
        <title>My Title</title>
        <link rel='canonical' href='http://mysite.com/profile' />
      </Helmet>
      <div className='app'>
        <header className='app-header'>
          <img src={logo} className='app-logo' alt='logo' />
          <p>
            Edit <code>src/pages/Profile/index.js</code> and save to reload.
          </p>
          <a className='app-link' href='https://reactjs.org' target='_blank' rel='noopener noreferrer'>
            Learn React
          </a>
        </header>
      </div>
    </>
  )
}

export default App
