import React from 'react'
import { mount } from 'enzyme'

import Profile from '~/pages/Profile'

describe('<Profile />', () => {
  test('should mount successfully', () => {
    const wrapper = mount(<Profile />)
    expect(wrapper.find('.app-header')).toHaveClassName('app-header')
    wrapper.unmount()
  })
})
