import React from 'react'
import { mount } from 'enzyme'

import Homepage from '~/pages/Homepage'

describe('<Homepage />', () => {
  test('should mount successfully', () => {
    const wrapper = mount(<Homepage />)
    expect(wrapper.find('.app-header')).toHaveClassName('app-header')
    wrapper.unmount()
  })
})
